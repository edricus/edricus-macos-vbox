# edricus-macos-vbox
![macos-screenshot](src/screenshot.png)
Fork du projet [MacOS-Virtualbox by myspaghetti](https://github.com/myspaghetti/macos-virtualbox)  
Ajout de la variable `--cpu-profile "Intel Core i7-2635QM` dans la fonction "configure_vm" à cause d'un echec avec mon profile CPU actuel (Ryzen 5 3500U).  

## Utilisation
```console
git clone https://gitlab.com/edricus/edricus-macos-vbox
cd edricus-macos-vbox
./macos.sh
```
<h2> Documentation </h2>

        NAME
Push-button installer of macOS on VirtualBox

        DESCRIPTION
The script downloads macOS High Sierra, Mojave, and Catalina from Apple servers
and installs them on VirtualBox 5.2, 6.0, and 6.1.  
The script doesn't install any closed-source additions or bootloaders.   
A default install requires the user press enter when prompted, less than ten times, to complete the installation.  
Systems with the package tesseract-ocr may automate the installation completely.  

        USAGE
    ./macos-guest-virtualbox.sh [STAGE]...   

The installation is divided into stages. Stage titles may be given as command-line arguments for the script.   
When the script is executed with no command-line arguments, each of the stages is performed in succession in the order listed:

    check_shell
    check_gnu_coreutils_prefix
    set_variables
    welcome
    check_dependencies
    prompt_delete_existing_vm
    create_vm
    check_default_virtual_machine
    prepare_macos_installation_files
    create_nvram_files
    create_macos_installation_files_viso
    configure_vm
    populate_basesystem_virtual_disk
    create_bootable_installer_virtual_disk
    populate_bootable_installer_virtual_disk
    create_target_virtual_disk
    populate_macos_target_disk
    prompt_delete_temporary_files

Other than the stages above, the command-line arguments "documentation",
"troubleshoot", and "and_all_subsequent_stages" are available. 
"troubleshoot" generates a file with system information, VirtualBox logs, and checksums for some installation files.   
"documentation" outputs the script's documentation.  
If "documentation" is the first argument, no other arguments are parsed.  
If the first argument is a stage title and the only other argument is
"and_all_subsequent_stages", then the speficied stage and all subsequent
stages are performed in succession in the order listed above;
otherwise, "and_all_subsequent_stages" does not perform any stages.  

The stage "check_shell" is always performed when the script loads.  

The stages "check_gnu_coreutils_prefix", "set_variables", and
"check_dependencies" are always performed when any stage title other than
"documentation" is specified as the first argument, and the rest of the
specified stages are performed only after the checks pass.  

        EXAMPLES
    ./macos-guest-virtualbox.sh create_vm configure_vm

The above command might be used to create and configure a virtual machine on a
new VirtualBox installation, then manually attach to the new virtual machine
an existing macOS disk image that was previously created by the script.  

    ./macos-guest-virtualbox.sh prompt_delete_temporary_files

The above command might be used when no more virtual machines need to be
installed, and the temporary files can be deleted.  

    ./macos-guest-virtualbox.sh configure_vm and_all_subsequent_stages

The above command might be used to resume the script stages after the stage
"configure_vm" failed.  

    ./macos-guest-virtualbox.sh \
    configure_vm create_nvram_files create_macos_installation_files_viso

The above command might be used to update the EFI and NVRAM variables required
for iCloud and iMessage connectivity and other Apple-connected apps.  

        Configuration
The script's default configuration is stored in the set_variables() function at
the top of the script. No manual configuration is required to use the script.  

The configuration may be manually edited either by editing the variable
assignment in set_variables() or by executing the following command:

    export macos_vm_vars_file=/path/to/variable_assignment_file

"variable_assignment_file" is a plain text file that contains zero or more
lines with a variable assignment for any variable specified in set_variables(),
for example macOS_release_name="HighSierra" or DmiSystemFamily="iMac"

        iCloud and iMessage connectivity
iCloud, iMessage, and other connected Apple services require a valid device
name and serial number, board ID and serial number, and other genuine
(or genuine-like) Apple parameters.   Assigning these parameters is not required
when installing or using macOS, only when connecting to the iCloud app,
iMessage, and other apps that authenticate the device with Apple.  

These are the variables that are usually required for iMessage connectivity:

    DmiSystemFamily    # Model name
    DmiSystemProduct   # Model identifier
    DmiBIOSVersion     # Boot ROM version
    DmiSystemSerial    # System serial number
    DmiSystemUuid      # Hardware unique identifier
    ROM                # ROM identifier, stored in NVRAM
    MLB                # Main Logic Board serial, stored in NVRAM
    DmiBoardSerial     # Main Logic Board serial, stored in EFI
    DmiBoardProduct    # Product (board) identifier
    SystemUUID         # System unique identifier, stored in NVRAM

These parameters may be manually set in the set_variables() function when the
"get_parameters_from_macOS_host" is set to "no", which is the default setting.   When
the script is executed on macOS and the variable "get_parameters_from_macOS_host" is
set to "yes", the script copies the parameters from the host.  

        Applying the EFI and NVRAM parameters
The EFI and NVRAM parameters may be set in the script before installation by
editing them at the top of the script.   NVRAM parameters may be applied after
the last step of the installation by resetting the virtual machine and booting
into the EFI Internal Shell.   When resetting or powering up the VM, immediately
press Esc when the VirtualBox logo appears.   This boots into the EFI Internal
Shell or the boot menu.   If the boot menu appears, select "Boot Manager" and
then "EFI Internal Shell" and then allow the startup.  nsh script to execute
automatically, applying the NVRAM variables before booting macOS.  

        Changing the EFI and NVRAM parameters after installation
The variables mentioned above may be edited and applied to an existing macOS
virtual machine by deleting the .  nvram file from the directory where the
virtual machine .  vbox file is stored, then executing the following
command and copying the generated files to the macOS EFI System Partition:

    ./macos-guest-virtualbox.sh \
    configure_vm create_nvram_files create_macos_installation_files_viso

After executing the command, attach the resulting VISO file to the virtual
machine's storage through VirtualBox Manager or VBoxManage.   Power up the VM
and boot macOS, then start Terminal and execute the following commands, making
sure to replace "[VISO_mountpoint]" with the correct path:

    mkdir ESP
    sudo su # this will prompt for a password
    diskutil mount -mountPoint ESP disk0s1
    cp -r /Volumes/[VISO_mountpoint]/ESP/* ESP/

After copying the files, boot into the EFI Internal Shell as described in the
section "Applying the EFI and NVRAM parameters".  

        Storage format
The script by default assigns a target virtual disk storage format of VDI.   This
format can be resized by VirtualBox as explained in the next section.   The other
available format, VMDK, cannot be resized by VirtualBox but can be attached to
a QEMU virtual machine for use with Linux KVM for better performance.  

        Storage size
The script by default assigns a target virtual disk storage size of 80GB, which
is populated to about 25GB on the host on initial installation.   After the
installation is complete, the VDI storage size may be increased.   First increase
the virtual disk image size through VirtualBox Manager or VBoxManage, then in
Terminal in the virtual machine execute the following command:
    sudo diskutil repairDisk disk0
After it completes, open Disk Utility and delete the "Free space" partition so
it allows the system APFS container to take up the available space, or if that
fails, execute the following command:
    sudo diskutil apfs resizeContainer disk1 0
Both Disk Utility and diskutil may fail and require successive resize attempts
separated by virtual machine reboots.  

        Primary display resolution
The following command assigns the virtual machine primary display resolution:
    VBoxManage setextradata "${vm_name}" \
    "VBoxInternal2/EfiGraphicsResolution" "${resolution}"
The following primary display resolutions are supported by macOS on VirtualBox:
  5120x2880  2880x1800  2560x1600  2560x1440  1920x1200  1600x1200  1680x1050
  1440x900   1280x800   1024x768   640x480
Secondary displays can have an arbitrary resolution.  

        Unsupported features
Developing and maintaining VirtualBox or macOS features is beyond the scope of
this script.   Some features may behave unexpectedly, such as USB device support,
audio support, FileVault boot password prompt support, and other features.  

        CPU profiles and CPUID settings (unsupported)
macOS does not support every CPU supported by VirtualBox.   If the macOS Base
System does not boot, try applying different CPU profiles to the virtual
machine with the VBoxManage commands described below.   First, while the
VM is powered off, set the guest's CPU profile to the host's CPU profile, then
try to boot the virtual machine:
    VBoxManage modifyvm "${vm_name}" --cpu-profile host
    VBoxManage modifyvm "${vm_name}" --cpuidremoveall
If booting fails, try assigning each of the preconfigured CPU profiles while
the VM is powered off with the following command:
    VBoxManage modifyvm "${vm_name}" --cpu-profile "${cpu_profile}"
Available CPU profiles:
  "Intel Xeon X5482 3.  20GHz"  "Intel Core i7-2635QM"  "Intel Core i7-3960X"
  "Intel Core i5-3570"  "Intel Core i7-5600U"  "Intel Core i7-6700K"
If booting fails after trying each preconfigured CPU profile, the host's CPU
requires specific macOS VirtualBox CPUID settings.  

        Performance and deployment (unsupported)
After successfully creating a working macOS virtual machine, consider importing
the virtual machine into more performant virtualization software, or packaging
it for configuration management platforms for automated deployment.   These
virtualization and deployment applications require additional configuration
that is beyond the scope of the script.  

QEMU with KVM is capable of providing virtual machine hardware passthrough
for near-native performance.   QEMU supports the VMDK virtual disk image format,
which can be configured to be created by the script, or converted from the
default VirtualBox VDI format into the VMDK format with the following command:
    VBoxManage clonehd --format vmdk source.  vdi target.  vmdk
QEMU and KVM require additional configuration that is beyond the scope of the
script.  

        VirtualBox Native Execution Manager (unsupported)
The VirtualBox Native Execution Manager (NEM) is an experimental VirtualBox
feature.   VirtualBox uses NEM when access to VT-x and AMD-V is blocked by
virtualization software or execution protection features such as Hyper-V,
Windows Sandbox, WSL2, memory integrity protection, and other software.  
macOS and the macOS installer have memory corruption issues under NEM
virtualization.   The script checks for NEM and exits with an error message if
NEM is detected.  

        Bootloaders (unsupported)
The macOS VirtualBox guest is loaded without extra bootloaders, but it is
compatible with OpenCore.   OpenCore requires additional configuration that is
beyond the scope of the script.  

        Display scaling (unsupported)
VirtualBox does not supply an EDID for its virtual display, and macOS does not
enable display scaling (high PPI) without an EDID.   The bootloader OpenCore can
inject an EDID which enables display scaling.  

        Audio (unsupported)
macOS may not support any built-in VirtualBox audio controllers.   The bootloader
OpenCore may be able to load open-source audio drivers in VirtualBox.  

        FileVault (unsupported)
The VirtualBox EFI implementation does not properly load the FileVault full disk
encryption password prompt upon boot.   The bootloader OpenCore is be able to
load the password prompt with the parameter "ProvideConsoleGop" set to "true".  

        Further information
Further information is available at the following URL:
        https://github.com/myspaghetti/macos-virtualbox
